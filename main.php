<?php
error_reporting( E_ALL ^ E_NOTICE );

include_once "classes/DB.class.php";
include_once "database_options.php";

try {
	DB::init( $dbOptions );

	switch( $_REQUEST[ 'action' ] ) {
		case 'AddNewUser':
			$result = DB::query(
				"INSERT INTO users ( name )
				 VALUES( '" . DB::escape( $_REQUEST[ 'user_name' ] ) . "' )"
			);

			if( $result ) {
				$response = "added";
			} else {
				$response = "fail";
			}
		break;

		case 'SaveChatMessages':
			$messages = json_decode( $_REQUEST[ 'messages' ] );

			foreach( $messages as $key => $message ) {
				$result = DB::query(
					"INSERT INTO messages ( room_id, sender_id, timestamp,
					                        msg_type, msg_data, link )
					 VALUES( {$message->room_id},     {$message->sender_id},
					         '{$message->timestamp}', {$message->msg_type}, '"
					         . DB::escape( $message->msg_data ) . "', '"
					         . DB::escape( $message->link )     . "' )"
				);

				if( $result ) {
					$response .= "{$key} sent\n";
				} else {
					$response .= "{$key} fail\n";
				}
			}
		break;

		case 'RequestRoomChatHistory':

			if( $_REQUEST[ 'timestamp' ] ) {
				$condition = "timestamp > '{$_REQUEST[ 'timestamp' ]}'";
			} elseif( $_REQUEST[ 'msg_id' ] ) {
				$condition = "msg_id = {$_REQUEST[ 'msg_id' ]}";
			}

			$result = DB::query(
				"SELECT *
				 FROM messages
				 WHERE room_id = {$_REQUEST[ 'room_id' ]} AND {$condition}"
			);

			if( $result ) {
				while( $row = $result->fetch_assoc() ) {
					$all_rows[] = $row;
				}

				$response = json_encode( $all_rows, JSON_FORCE_OBJECT );

				$result->close();
			} else {
				$response = "fail";
			}

		break;
	}

	echo $response;

} catch( Exception $error ) {
	die( json_encode( array( 'error' => $error->getMessage() ) ) );
}

?>
